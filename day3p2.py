TREE = "#"
STEP = (3, 1)

# load input

inp_map = []

try:
    while True:
        inp_map += [[input()]]
except EOFError:
    pass

# for row in inp_map:
#     print(*row)

# start traverse


def traverse(STEP):
    tree_counter = 0
    pos = [0, 0]

    end = True

    while end:
        # apply step
        pos[0] += STEP[0]
        pos[0] = pos[0] % len(inp_map[0][0])
        pos[1] += STEP[1]
        print(pos)
        # if outside of map end
        if pos[1] > len(inp_map) - 1:
            end = False
        elif inp_map[pos[1]][0][pos[0]] == TREE:
            tree_counter += 1

    print(tree_counter)
    return tree_counter


res = traverse(STEP) * traverse((1, 1)) * traverse((5, 1)) * \
    traverse((7, 1)) * traverse((1, 2))
print(res)
