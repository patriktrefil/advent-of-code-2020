# load input

inp = []

try:
    while True:
        inp += [int(input())]
except EOFError:
    pass

for i, num in enumerate(inp):
    for j, p in enumerate(inp[i + 1:]):
        for y in inp[j + 1:]:
            if num + p + y == 2020:
                print(num * p * y)
                exit(0)

print("None found")

