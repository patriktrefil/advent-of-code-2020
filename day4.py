def process_passport(passport: str) -> bool:
    validity = False
    REQUIRED = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
    # print(passport)
    # data = {item[0]: item[1] for item in [item.split(":") for item in passport.split(" ")]}
    keys = {item.split(":")[0] for item in passport.split()}

    counter = 0
    for key in keys:
        if key in REQUIRED: 
            counter += 1

    if counter == len(REQUIRED):
        validity = True

    return validity


counter = 0

try:
    while True:
        # load passport

        line = None
        passport = ""

        while line != "":
            line = input()
            passport += " " + line

        if process_passport(passport) is True:
            counter += 1
except EOFError:
    if process_passport(passport) is True:
        counter += 1

print(counter)
