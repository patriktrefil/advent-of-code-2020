res:set = set()

try:
    while True:
        line = input()

        rows, columns = line[:7], line[7:]

        span = (0, 127)

        for char in rows:
            if char == "F":
                span = (span[0], span[0] + (span[1] - span[0]) // 2)
            elif char == "B":
                span = (span[1] - (span[1] - span[0]) // 2, span[1])

        row = span[0]

        span = (0, 8)
        for char in columns:
            if char == "L":
                span = (span[0], span[0] + (span[1] - span[0]) // 2)
            elif char == "R":
                span = (span[1] - (span[1] - span[0]) // 2, span[1])

        col = span[0]

        res.add(row * 8 + col)
except EOFError:
    print(max(res))