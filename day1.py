# load input

inp = []

try:
    while True:
        inp += [int(input())]
except EOFError:
    pass

for i, num in enumerate(inp):
    for p in inp[i + 1:]:
        if num + p == 2020:
            print(num * p)
            exit(0)

print("None found")

