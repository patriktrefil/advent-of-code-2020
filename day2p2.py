# load input

glob_counter = 0

try:
    while True:
        inp = input().split(":")
        pos, letter = inp[0].split(" ")
        pos = tuple([int(item) - 1 for item in pos.split("-")])
        text = inp[1].strip()
        # print(pos[0], pos[1], letter, text)
        first = false = False
        try:
            first = text[pos[0]] == letter and text[pos[1]] != letter
        except IndexError:
            pass
        try:
            second = text[pos[0]] != letter and text[pos[1]] == letter
        except IndexError:
            pass

        if first or second:
            glob_counter += 1
            print(text)
except EOFError:
    pass

print(glob_counter)
