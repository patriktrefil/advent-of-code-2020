# load input

glob_counter = 0

try:
    while True:
        inp = input().split(":")
        repeat, letter = inp[0].split(" ")
        repeat = [int(item) for item in repeat.split("-")]
        text = inp[1]
        print(repeat[0], repeat[1], letter, text)

        counter = 0

        for char in text:
            if char == letter:
                counter += 1

        if counter >= repeat[0] and counter <= repeat[1]:
            glob_counter += 1
except EOFError:
    pass

print(glob_counter)
