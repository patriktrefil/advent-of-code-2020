def process_passport(passport: str) -> bool:
    def check_height(data):
        if data[-2:] == "cm":
            height = int(data[:-2])
            if height >= 150 and height <= 193:
                return True
        elif data[-2:] == "in":
            height = int(data[:-2])
            if height >= 59 and height <= 76:
                return True
        return False

    def check_hair_color(data):

        if data[0] != "#":
            return False

        POSSIBLE = {char for char in "abcdef0123456789"}

        for char in data[1:]:
            if char not in POSSIBLE:
                return False
        return True

    def check_eye_color(data):
        POSSIBLE = {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}
        if data in POSSIBLE:
            return True
        else:
            return False

    def check_pid(data):
        if len(data) != 9:
            return False

        POSSIBLE = {str(n) for n in range(10)}
        for n in data:
            if n not in POSSIBLE:
                return False
        return True

    validity = False
    REQUIRED = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
    # print(passport)
    data = {item[0]: item[1] for item in [item.split(":") for item in passport.split()]}

    counter = 0
    for key in data.keys():
        if key in REQUIRED: 
            counter += 1

    if counter == len(REQUIRED):
        if int(data["byr"]) >= 1920 and int(data["byr"]) <= 2002:
            if int(data["iyr"]) >= 2010 and int(data["iyr"]) <= 2020:
                if int(data["eyr"]) >= 2020 and int(data["eyr"]) <= 2030:
                    if check_height(data["hgt"]):
                        if check_hair_color(data["hcl"]):
                            if check_eye_color(data["ecl"]):
                                if check_pid(data["pid"]):
                                    validity = True

    return validity


counter = 0

try:
    while True:
        # load passport

        line = None
        passport = ""

        while line != "":
            line = input()
            passport += " " + line

        if process_passport(passport) is True:
            counter += 1
except EOFError:
    if process_passport(passport) is True:
        counter += 1

print(counter)
